package trachtenberg.ari.demoproject;

/**
 * Does addition.
 */
public class Plusser
{
    /**
     * @param aa One addend
     * @param bb Another addend
     * @return The sum of aa and bb
     */
    public static int plus(int aa, int bb) {
        return aa+bb;
    }
}
